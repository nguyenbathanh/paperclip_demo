# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150604090907) do

  create_table "admins", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "admins", ["email"], name: "index_admins_on_email", unique: true
  add_index "admins", ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true

  create_table "dusers", force: true do |t|
    t.integer  "publication_id"
    t.string   "login_email",                                                     null: false
    t.string   "crypted_password",                                                null: false
    t.string   "password_salt"
    t.string   "persistence_token",                                               null: false
    t.string   "first_name"
    t.string   "last_name"
    t.string   "description"
    t.string   "avatar_url"
    t.integer  "login_count",                                     default: 0
    t.datetime "last_login_at"
    t.boolean  "can_update_issues",                               default: true
    t.datetime "iss_timestamp"
    t.string   "generated_password"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "uif_public_documents",                            default: false
    t.boolean  "uif_enable_subscriber_edit",                      default: false
    t.boolean  "uif_edit_landingpage_tags",                       default: false
    t.boolean  "testdrive_animation",                             default: false
    t.integer  "testdrivecode_id"
    t.integer  "number_of_logins"
    t.boolean  "testdrive_rndemail",                              default: false
    t.boolean  "testdrive_msgaassign",                            default: false
    t.integer  "update_counter",                                  default: 0
    t.integer  "update_counter_issues",                           default: 0
    t.integer  "update_counter_subscribers",                      default: 0
    t.string   "account_identifier"
    t.integer  "limit_subscriber_number",                         default: 0
    t.string   "language",                              limit: 5
    t.integer  "origin_lp"
    t.boolean  "uif_edit_forceremove",                            default: true
    t.string   "external_partner"
    t.boolean  "uif_subscriber_derigistration",                   default: true
    t.boolean  "uif_document_expire",                             default: true
    t.boolean  "uif_documentscan",                                default: false
    t.boolean  "uif_public_documents_by_duser",                   default: true
    t.boolean  "uif_log_client_events",                           default: false
    t.boolean  "log_client_events"
    t.boolean  "subscriber_deregistration_allowed"
    t.boolean  "uif_geo_settings",                                default: true
    t.boolean  "disable_subscribers",                             default: false
    t.integer  "client_eventlog_wait_min"
    t.integer  "client_eventlog_wait_count"
    t.boolean  "client_eventlog_geo_enabled",                     default: true
    t.boolean  "appinazor_button_enabled",                        default: true
    t.boolean  "appinazor_app_created",                           default: false
    t.boolean  "uif_publication_date",                            default: false
    t.boolean  "login_is_email",                                  default: true
    t.string   "notification_email"
    t.integer  "external_origin"
    t.string   "external_reference"
    t.string   "origin_country",                        limit: 5
    t.boolean  "uif_goldplate",                                   default: true
    t.string   "external_order_number"
    t.boolean  "no_subscribers_default_public_visible",           default: false
    t.boolean  "uif_document_availability",                       default: true
    t.boolean  "access_all_dusers",                               default: false
    t.boolean  "access_all_subscribers",                          default: false
    t.boolean  "access_all_issues",                               default: false
    t.boolean  "uif_issue_email_forwarding",                      default: true
    t.boolean  "uif_audio_enabled",                               default: true
    t.boolean  "uif_download_limit",                              default: true
    t.boolean  "uif_emailfeature_ta",                             default: true
    t.integer  "parent_id"
    t.boolean  "uif_subordinate_dusers",                          default: false
    t.boolean  "uif_push_notifications_enabled",                  default: false
    t.boolean  "uif_push_private",                                default: true
    t.boolean  "uif_push_public",                                 default: true
    t.integer  "issues_sort_mode"
    t.boolean  "uif_feeds_private",                               default: false
    t.boolean  "uif_headline_private",                            default: false
    t.boolean  "has_agreed_to_push_policy",                       default: false
    t.integer  "headline_ui_type",                                default: 1
    t.boolean  "uif_feeds_public",                                default: true
    t.boolean  "uif_headline_public",                             default: false
    t.boolean  "headline_info",                                   default: false
    t.boolean  "uif_sorting",                                     default: true
    t.boolean  "uif_headline_url",                                default: false
    t.boolean  "uif_feed_image_upload",                           default: false
  end

  add_index "dusers", ["account_identifier"], name: "index_dusers_on_account_identifier"

  create_table "friends", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
  end

  create_table "issues", force: true do |t|
    t.string   "title"
    t.string   "uniqueID"
    t.string   "uniquePublicationID"
    t.string   "itunesID"
    t.integer  "publication_id"
    t.integer  "serverVersion"
    t.boolean  "available_for_devices",                             default: false
    t.boolean  "public_visible",                                    default: false
    t.boolean  "free_download",                                     default: false
    t.text     "issueXML"
    t.text     "issueFilesXML"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "subscription_index"
    t.boolean  "subscription_enabled",                              default: false
    t.integer  "duser_id"
    t.boolean  "distributed_by_duser",                              default: false
    t.string   "smallcover_url"
    t.boolean  "document_found",                                    default: false
    t.boolean  "newsstand_subscription_enabled",                    default: false
    t.datetime "publication_date"
    t.boolean  "npc_distribution_ipad",                             default: true
    t.boolean  "npc_distribution_iphone",                           default: true
    t.boolean  "npc_distribution_androidphone",                     default: true
    t.boolean  "npc_distribution_androidtablet",                    default: true
    t.boolean  "webcover_generating",                               default: false
    t.integer  "update_counter",                                    default: 0
    t.boolean  "force_remove",                                      default: true
    t.integer  "change_ts_document"
    t.integer  "change_ts_cover"
    t.integer  "page1_offset"
    t.integer  "change_ts_title"
    t.integer  "change_ts_settings"
    t.datetime "expiration"
    t.integer  "change_ts_contenttable"
    t.integer  "change_ts_taparea"
    t.boolean  "client_scan_urls",                                  default: false
    t.boolean  "client_scan_emails",                                default: false
    t.boolean  "client_scan_ordernumbers",                          default: false
    t.string   "client_scan_numbertemplate",             limit: 40
    t.boolean  "public_by_duser",                                   default: false
    t.boolean  "log_client_events"
    t.string   "play_id"
    t.boolean  "show_expiration_icon",                              default: false
    t.integer  "change_ts_video"
    t.integer  "change_ts_gallery"
    t.integer  "change_ts_gallery_image"
    t.boolean  "client_no_local_encryption",                        default: false
    t.boolean  "requires_server_key",                               default: false
    t.string   "client_server_key"
    t.boolean  "client_opens_highlight_urls_internally",            default: true
    t.string   "client_ordernumber_baseurl"
    t.boolean  "hidden",                                            default: false
    t.datetime "availability"
    t.integer  "change_ts_audio"
    t.integer  "download_limit",                                    default: 0
    t.boolean  "issue_email_forwarding",                            default: false
    t.boolean  "open_externally",                                   default: false
    t.string   "email_forwarding_to"
    t.string   "email_forwarding_cc"
    t.string   "email_forwarding_bcc"
    t.string   "email_forwarding_subject"
    t.text     "email_forwarding_message"
    t.boolean  "available_own_app",                                 default: true
    t.boolean  "available_partner_app"
    t.integer  "default_partner_tile"
  end

  add_index "issues", ["uniqueID", "uniquePublicationID"], name: "index_issues_on_uniqueID_and_uniquePublicationID", unique: true

  create_table "publications", force: true do |t|
    t.string   "title"
    t.string   "uniqueID"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "client_logo_url"
    t.string   "client_title"
    t.string   "duser_registration_url"
    t.integer  "subscriber_default_duser_id"
    t.boolean  "subscriber_selfregistration_enabled", default: false
    t.boolean  "log_client_events"
    t.string   "client_host"
    t.string   "client_logo_url2"
    t.string   "key_read"
    t.string   "key_read_secret"
    t.string   "key_write"
    t.string   "key_write_secret"
    t.string   "shared_secret_itunes"
    t.boolean  "subscriber_code_registration",        default: true
    t.string   "client_access_password"
    t.integer  "storage_issue_processmode",           default: 1
    t.boolean  "testdrive_animation",                 default: true
    t.boolean  "padcloud_apn",                        default: false
    t.boolean  "storage_duseridpaths",                default: false
    t.boolean  "storage_localfiles",                  default: false
    t.boolean  "precreated_subscribers",              default: false
    t.boolean  "nouploadpwd",                         default: true
    t.string   "verification_form_prepended_text"
    t.string   "verification_form_appended_text"
    t.boolean  "secure_password",                     default: false
    t.boolean  "subscriber_deregistration_allowed",   default: true
    t.boolean  "appinazor_button_enabled",            default: true
    t.boolean  "push_on_public_doc_enabled",          default: true
    t.boolean  "enable_taparea_audio",                default: true
    t.boolean  "download_limit_enabled",              default: true
    t.boolean  "mailing_feature_enabled",             default: true
    t.boolean  "push_server_enabled",                 default: false
    t.boolean  "push_on_public_by_duser_doc_enabled", default: true
    t.integer  "headline_ui_type",                    default: 1
    t.boolean  "has_lock_screen",                     default: false
    t.string   "hardcoded_pass"
    t.boolean  "subscriber_multi_login_enabled",      default: false
  end

end
