class CreateDusers < ActiveRecord::Migration
  def change
    create_table :dusers do |t|
      t.integer  "publication_id"
      t.string   "login_email",                           limit: 255,                 null: false
      t.string   "crypted_password",                      limit: 255,                 null: false
      t.string   "password_salt",                         limit: 255
      t.string   "persistence_token",                     limit: 255,                 null: false
      t.string   "first_name",                            limit: 255
      t.string   "last_name",                             limit: 255
      t.string   "description",                           limit: 255
      t.string   "avatar_url",                            limit: 255
      t.integer  "login_count",                                       default: 0
      t.datetime "last_login_at"
      t.boolean  "can_update_issues",                                 default: true
      t.datetime "iss_timestamp"
      t.string   "generated_password",                    limit: 255
      t.datetime "created_at"
      t.datetime "updated_at"
      t.boolean  "uif_public_documents",                              default: false
      t.boolean  "uif_enable_subscriber_edit",                        default: false
      t.boolean  "uif_edit_landingpage_tags",                         default: false
      t.boolean  "testdrive_animation",                               default: false
      t.integer  "testdrivecode_id"
      t.integer  "number_of_logins"
      t.boolean  "testdrive_rndemail",                                default: false
      t.boolean  "testdrive_msgaassign",                              default: false
      t.integer  "update_counter",                                    default: 0
      t.integer  "update_counter_issues",                             default: 0
      t.integer  "update_counter_subscribers",                        default: 0
      t.string   "account_identifier",                    limit: 255
      t.integer  "limit_subscriber_number",                           default: 0
      t.string   "language",                              limit: 5
      t.integer  "origin_lp"
      t.boolean  "uif_edit_forceremove",                              default: true
      t.string   "external_partner",                      limit: 255
      t.boolean  "uif_subscriber_derigistration",                     default: true
      t.boolean  "uif_document_expire",                               default: true
      t.boolean  "uif_documentscan",                                  default: false
      t.boolean  "uif_public_documents_by_duser",                     default: true
      t.boolean  "uif_log_client_events",                             default: false
      t.boolean  "log_client_events"
      t.boolean  "subscriber_deregistration_allowed"
      t.boolean  "uif_geo_settings",                                  default: true
      t.boolean  "disable_subscribers",                               default: false
      t.integer  "client_eventlog_wait_min"
      t.integer  "client_eventlog_wait_count"
      t.boolean  "client_eventlog_geo_enabled",                       default: true
      t.boolean  "appinazor_button_enabled",                          default: true
      t.boolean  "appinazor_app_created",                             default: false
      t.boolean  "uif_publication_date",                              default: false
      t.boolean  "login_is_email",                                    default: true
      t.string   "notification_email",                    limit: 255
      t.integer  "external_origin"
      t.string   "external_reference",                    limit: 255
      t.string   "origin_country",                        limit: 5
      t.boolean  "uif_goldplate",                                     default: true
      t.string   "external_order_number",                 limit: 255
      t.boolean  "no_subscribers_default_public_visible",             default: false
      t.boolean  "uif_document_availability",                         default: true
      t.boolean  "access_all_dusers",                                 default: false
      t.boolean  "access_all_subscribers",                            default: false
      t.boolean  "access_all_issues",                                 default: false
      t.boolean  "uif_issue_email_forwarding",                        default: true
      t.boolean  "uif_audio_enabled",                                 default: true
      t.boolean  "uif_download_limit",                                default: true
      t.boolean  "uif_emailfeature_ta",                               default: true
      t.integer  "parent_id"
      t.boolean  "uif_subordinate_dusers",                            default: false
      t.boolean  "uif_push_notifications_enabled",                    default: false
      t.boolean  "uif_push_private",                                  default: true
      t.boolean  "uif_push_public",                                   default: true
      t.integer  "issues_sort_mode"
      t.boolean  "uif_feeds_private",                                 default: false
      t.boolean  "uif_headline_private",                              default: false
      t.boolean  "has_agreed_to_push_policy",                         default: false
      t.integer  "headline_ui_type",                                  default: 1
      t.boolean  "uif_feeds_public",                                  default: true
      t.boolean  "uif_headline_public",                               default: false
      t.boolean  "headline_info",                                     default: false
      t.boolean  "uif_sorting",                                       default: true
      t.boolean  "uif_headline_url",                                  default: false
      t.boolean  "uif_feed_image_upload",                             default: false
    end

    add_index "dusers", ["account_identifier"], name: "index_dusers_on_account_identifier"
  end

  def down
    drop_table :dusers
  end
end
