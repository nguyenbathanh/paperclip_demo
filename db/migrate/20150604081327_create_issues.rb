class CreateIssues < ActiveRecord::Migration
  def change
    create_table :issues do |t|
      t.string   "title",                                  limit: 255
      t.string   "uniqueID",                               limit: 255
      t.string   "uniquePublicationID",                    limit: 255
      t.string   "itunesID",                               limit: 255
      t.integer  "publication_id"
      t.integer  "serverVersion"
      t.boolean  "available_for_devices",                              default: false
      t.boolean  "public_visible",                                     default: false
      t.boolean  "free_download",                                      default: false
      t.text     "issueXML"
      t.text     "issueFilesXML"
      t.datetime "created_at"
      t.datetime "updated_at"
      t.integer  "subscription_index"
      t.boolean  "subscription_enabled",                               default: false
      t.integer  "duser_id"
      t.boolean  "distributed_by_duser",                               default: false
      t.string   "smallcover_url",                         limit: 255
      t.boolean  "document_found",                                     default: false
      t.boolean  "newsstand_subscription_enabled",                     default: false
      t.datetime "publication_date"
      t.boolean  "npc_distribution_ipad",                              default: true
      t.boolean  "npc_distribution_iphone",                            default: true
      t.boolean  "npc_distribution_androidphone",                      default: true
      t.boolean  "npc_distribution_androidtablet",                     default: true
      t.boolean  "webcover_generating",                                default: false
      t.integer  "update_counter",                                     default: 0
      t.boolean  "force_remove",                                       default: true
      t.integer  "change_ts_document"
      t.integer  "change_ts_cover"
      t.integer  "page1_offset"
      t.integer  "change_ts_title"
      t.integer  "change_ts_settings"
      t.datetime "expiration"
      t.integer  "change_ts_contenttable"
      t.integer  "change_ts_taparea"
      t.boolean  "client_scan_urls",                                   default: false
      t.boolean  "client_scan_emails",                                 default: false
      t.boolean  "client_scan_ordernumbers",                           default: false
      t.string   "client_scan_numbertemplate",             limit: 40
      t.boolean  "public_by_duser",                                    default: false
      t.boolean  "log_client_events"
      t.string   "play_id",                                limit: 255
      t.boolean  "show_expiration_icon",                               default: false
      t.integer  "change_ts_video"
      t.integer  "change_ts_gallery"
      t.integer  "change_ts_gallery_image"
      t.boolean  "client_no_local_encryption",                         default: false
      t.boolean  "requires_server_key",                                default: false
      t.string   "client_server_key",                      limit: 255
      t.boolean  "client_opens_highlight_urls_internally",             default: true
      t.string   "client_ordernumber_baseurl",             limit: 255
      t.boolean  "hidden",                                             default: false
      t.datetime "availability"
      t.integer  "change_ts_audio"
      t.integer  "download_limit",                                     default: 0
      t.boolean  "issue_email_forwarding",                             default: false
      t.boolean  "open_externally",                                    default: false
      t.string   "email_forwarding_to",                    limit: 255
      t.string   "email_forwarding_cc",                    limit: 255
      t.string   "email_forwarding_bcc",                   limit: 255
      t.string   "email_forwarding_subject",               limit: 255
      t.text     "email_forwarding_message"
      t.boolean  "available_own_app",                                  default: true
      t.boolean  "available_partner_app"
      t.integer  "default_partner_tile"
    end
    add_index "issues", ["uniqueID", "uniquePublicationID"], name: "index_issues_on_uniqueID_and_uniquePublicationID", unique: true
  end
  def down
    drop_table :issues
  end
end
