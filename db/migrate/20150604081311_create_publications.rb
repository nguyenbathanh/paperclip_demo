class CreatePublications < ActiveRecord::Migration
  def change
    create_table :publications do |t|
      t.string   "title",                               limit: 255
      t.string   "uniqueID",                            limit: 255
      t.datetime "created_at"
      t.datetime "updated_at"
      t.string   "client_logo_url",                     limit: 255
      t.string   "client_title",                        limit: 255
      t.string   "duser_registration_url",              limit: 255
      t.integer  "subscriber_default_duser_id"
      t.boolean  "subscriber_selfregistration_enabled",             default: false
      t.boolean  "log_client_events"
      t.string   "client_host",                         limit: 255
      t.string   "client_logo_url2",                    limit: 255
      t.string   "key_read",                            limit: 255
      t.string   "key_read_secret",                     limit: 255
      t.string   "key_write",                           limit: 255
      t.string   "key_write_secret",                    limit: 255
      t.string   "shared_secret_itunes",                limit: 255
      t.boolean  "subscriber_code_registration",                    default: true
      t.string   "client_access_password",              limit: 255
      t.integer  "storage_issue_processmode",                       default: 1
      t.boolean  "testdrive_animation",                             default: true
      t.boolean  "padcloud_apn",                                    default: false
      t.boolean  "storage_duseridpaths",                            default: false
      t.boolean  "storage_localfiles",                              default: false
      t.boolean  "precreated_subscribers",                          default: false
      t.boolean  "nouploadpwd",                                     default: true
      t.string   "verification_form_prepended_text",    limit: 255
      t.string   "verification_form_appended_text",     limit: 255
      t.boolean  "secure_password",                                 default: false
      t.boolean  "subscriber_deregistration_allowed",               default: true
      t.boolean  "appinazor_button_enabled",                        default: true
      t.boolean  "push_on_public_doc_enabled",                      default: true
      t.boolean  "enable_taparea_audio",                            default: true
      t.boolean  "download_limit_enabled",                          default: true
      t.boolean  "mailing_feature_enabled",                         default: true
      t.boolean  "push_server_enabled",                             default: false
      t.boolean  "push_on_public_by_duser_doc_enabled",             default: true
      t.integer  "headline_ui_type",                                default: 1
      t.boolean  "has_lock_screen",                                 default: false
      t.string   "hardcoded_pass",                      limit: 255
      t.boolean  "subscriber_multi_login_enabled",                  default: false
    end
  end

  def down
    drop_table :publications
  end
end
