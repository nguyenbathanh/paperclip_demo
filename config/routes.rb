PaperclipDemo::Application.routes.draw do
  devise_for :admin
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  resources :friends
  root :to => 'friends#index'
end
